# UI ASSIGNMENT

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development server with proxy

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Mock data server

Run `npm run json-serve` for a mock json server.

## Start project in dev mode
Run `npm install` to install the node modules.
Run `npm run json-serve` for a mock json server, to start the mock JSON server.
Run `npm start` for a dev server.





