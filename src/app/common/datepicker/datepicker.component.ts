import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { DatepickerOptions } from 'ng2-datepicker';
@Component({
  selector: 'custom-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DatepickerComponent implements OnInit {
  @Input() datePickerOptions: DatepickerOptions;
  constructor() { }

  ngOnInit() {
    this.options = Object.assign( this.options,this.datePickerOptions);
  }

  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2025,
    displayFormat: 'MMM DD YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    minDate: new Date(Date.now()), // Minimal selectable date
    barTitleIfEmpty: 'Click to select a date',
    placeholder: 'Click to select a date', // HTML input placeholder attribute (default: '')
    addClass: 'form-control', // Optional, value to pass on to [ngClass] on the input field
    addStyle: {}, // Optional, value to pass to [ngStyle] on the input field
    fieldId: 'custom-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
    useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
  };


}
