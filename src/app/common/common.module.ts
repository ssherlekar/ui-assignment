import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgDatepickerModule } from 'ng2-datepicker';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { RangesliderComponent } from './rangeslider/rangeslider.component';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
@NgModule({
  declarations: [
    DatepickerComponent,
    RangesliderComponent
  ],
  imports: [
    BrowserModule,
    NgSelectModule,
    NgDatepickerModule,
    IonRangeSliderModule
  ],
  providers: [],
  bootstrap: [DatepickerComponent,RangesliderComponent],
  exports:[DatepickerComponent,RangesliderComponent]
})
export class CommonModule { }
