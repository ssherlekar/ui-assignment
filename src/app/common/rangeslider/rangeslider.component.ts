import { Component, OnInit,Input,ViewEncapsulation,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'custom-rangeslider',
  templateUrl: './rangeslider.component.html',
  styleUrls: ['./rangeslider.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RangesliderComponent implements OnInit {
  @Input() sliderOptions;
  @Output() onSlideComplete: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onSlideFinish(data){
    this.onSlideComplete.emit(data);
  }
}
