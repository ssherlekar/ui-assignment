import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {

  }

  getCities() {
    return this.http.get('/api/cities');
  }

  getflights() {
    return this.http.get('/api/flights');
  }
}
