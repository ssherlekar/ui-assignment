import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppComponent } from './app.component';
import { FlightSearchComponent } from './flight-search/flight-search.component';
import { FlightResultComponent } from './flight-result/flight-result.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgDatepickerModule } from 'ng2-datepicker';
import { ApiService } from './api.service';
import { CommonModule } from './common/common.module';
import { AppService } from './app.service';
import { FlightInfoComponent } from './flight-result/flight-info/flight-info.component';

@NgModule({
  declarations: [
    AppComponent,
    FlightSearchComponent,
    FlightResultComponent,
    FlightInfoComponent
  ],
  imports: [
    BrowserModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [ApiService,AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
