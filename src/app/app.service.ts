import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  @Output() searchFlightEmitter: EventEmitter<any> = new EventEmitter();
  @Output() priceFilterEmitter: EventEmitter<any> = new EventEmitter();

  constructor() { }

  searchFlight(searchQuery) {
    this.searchFlightEmitter.emit(searchQuery);
  }

  getFlightFilter() {
    return this.searchFlightEmitter;
  }

  priceFilter(searchQuery) {
    this.priceFilterEmitter.emit(searchQuery);
  }

  getPriceFilter() {
    return this.priceFilterEmitter;
  }

  filterFlightresult(filter, response) {
    let city = { source: filter.source.cityCode, destination: filter.destination.cityCode };
    let price = { min: filter.minPrice, max: filter.maxPrice };
    let departureDate = filter.departureDate ? this.formatDate(filter.departureDate) : null;
    let result = [];

    if (city.source && city.destination) {
      result = response.filter((res) => {
        res.selected = false;
        return (res.source === city.source && res.destination === city.destination)
      })
    }
    if (price.min && price.max) {
      result = result.filter((res) => {
        return (res.price >= price.min && res.price <= price.max);
      });
    }
    if (departureDate) {
      result = result.filter((res) => {
        var resDeparture = this.formatDate(res.departure);
        if (departureDate === resDeparture) return res;
      })
    }

    return result;
  }

  formatDate(date) {
    var d: any = new Date(date);
    return d.getDay() + '/' + (parseInt(d.getMonth()) + 1) + '/' + d.getFullYear();
  }

}
