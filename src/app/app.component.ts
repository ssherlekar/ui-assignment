import { Component, ViewEncapsulation } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  constructor(private _appService: AppService) { }
  title = 'assignment-app';
  public priceFilterValue;
  public priceFilterOptions = {
    minValue: 0,
    maxValue: 10000,
    from: 0,
    fromMin: 1000,
    toMin: 3000,
    toMax: 15000,
    grid: true
  }

  priceFilterChanged(query) {
    this._appService.priceFilter(query);
  }
  toggleTab() {
    let elem = <HTMLElement>event.target;
    let elemNext = elem.nextElementSibling;
    elemNext.classList.remove('close');
    if (elemNext.classList.contains('open')) {
      elemNext.classList.remove('open');
      elemNext.classList.add('close');
      elem.classList.remove('open');
      elem.classList.add('close');
      return;
    }
    elem.classList.add('open')
    elemNext.classList.add('open');
  }
}
