import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ApiService } from '../api.service';
import { AppService } from '../app.service';

export interface SearchCriteria {
  source: {},
  destination: {},
  departureDate: string,
  returnDate?: string
  minPrice?: string,
  maxPrice?: string,
  trip: string,
  passenger: number
}

@Component({
  selector: 'flight-search',
  templateUrl: './flight-search.component.html',
  styleUrls: ['./flight-search.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FlightSearchComponent implements OnInit {

  constructor(private _apiService: ApiService, private _appService: AppService) {
    this._appService.getPriceFilter().subscribe((res) => {
      this.searchFlights(res)
    })
  }

  public searchCriteria: SearchCriteria = {
    source: {},
    destination: {},
    departureDate: "",
    trip: "single",
    passenger: 1
  }

  public cities: Array<{}> = [];
  public returnTrip: boolean = false;
  public passengers: Array<{ id: number, count: number }> = [{ id: 1, count: 1 }, { id: 2, count: 2 }, { id: 3, count: 3 }, { id: 4, count: 4 }]

  public departureDateOptions = {
    barTitleIfEmpty: 'Click to select departure date',
    placeholder: 'Click to select departure date',
    selectedDate: ''
  }

  public returnDateOptions = {
    barTitleIfEmpty: 'Click to select return date',
    placeholder: 'Click to select return date',
    selectedDate: ''
  }

  ngOnInit() {
    this._apiService.getCities().subscribe((res: any) => {
      this.cities = res;
    });
  }

  selectSource(source) {
    this.searchCriteria.source = source;
  }

  selectDestination(destination) {
    this.searchCriteria.destination = destination;
  }

  selectPassengers(passenger) {
    this.searchCriteria.passenger = passenger.count;
  }

  searchFlights(priceFilterValue) {
    this.searchCriteria.departureDate = this.departureDateOptions.selectedDate;
    this.returnDateOptions.selectedDate && (this.searchCriteria.returnDate = this.returnDateOptions.selectedDate);
    if (!this.searchCriteria.departureDate || (this.returnTrip && !this.searchCriteria.returnDate)) {
      const msg =  this.returnTrip?'Please Select Return Date': 'Please Select Departure Date';
      alert(msg);
      return;
    }

    if (priceFilterValue) {
      this.searchCriteria.minPrice = priceFilterValue.from;
      this.searchCriteria.maxPrice = priceFilterValue.to;
    }
    this.searchCriteria.trip = this.returnTrip ? 'double' : 'single';
    this._appService.searchFlight(this.searchCriteria);
  }
}
