import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'flight-info',
  templateUrl: './flight-info.component.html',
  styleUrls: ['./flight-info.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlightInfoComponent implements OnInit {

  constructor() { }
  @Input() flight;
  @Input() returnTrip: boolean;
  ngOnInit() {
  }

}
