import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { ApiService } from '../api.service';
import { AppService } from '../app.service';

@Component({
  selector: 'flight-result',
  templateUrl: './flight-result.component.html',
  styleUrls: ['./flight-result.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlightResultComponent implements OnInit {

  constructor(private _apiService: ApiService, private _appService: AppService) {

  }
  public flightList: any = [];
  public flightFilter: any;
  public selectedFlight: { dFprice: number, currency: string, rFprice: number, ttlPrice: number } = { dFprice: null, currency: '', rFprice: null, ttlPrice: null }
  public flightsAvailable: boolean = true;

  ngOnInit() {
    this._appService.getFlightFilter().subscribe((filter) => {

      this.flightFilter = JSON.parse(JSON.stringify(filter));
      this.flightFilter.departureDate = this.flightFilter.departureDate ? this._appService.formatDate(this.flightFilter.departureDate) : null;
      this.flightFilter.returnDate = this.flightFilter.returnDate ? this._appService.formatDate(this.flightFilter.departureDate) : null;

      this._apiService.getflights().subscribe((response: any) => {
        this.selectedFlight.rFprice = 0;
        this.selectedFlight.dFprice = 0;
        this.flightsAvailable = true;
        this.flightList.departureFlightInfo = this._appService.filterFlightresult(filter, response);
        if (filter.trip === 'double') {
          let filterForReturnTrip = JSON.parse(JSON.stringify(filter));
          let filterCity = filterForReturnTrip.source;
          filterForReturnTrip.source = filterForReturnTrip.destination;
          filterForReturnTrip.destination = filterCity;
          filterForReturnTrip.departureDate = filterForReturnTrip.returnDate;
          this.flightList.returnFlightInfo = this._appService.filterFlightresult(filterForReturnTrip, response);
          !this.flightList.returnFlightInfo.length && (this.flightsAvailable = false)
        }
        !this.flightList.departureFlightInfo.length && (this.flightsAvailable = false);
      });
    });

  }

  selectDepartureFlight(flight, departureFlightList, index) {
    this._selectFLight(flight, departureFlightList, index);
    this.selectedFlight.dFprice = flight.selected ? flight.price : 0;
    this.selectedFlight.currency = flight.currency;
  }

  selectReturnFlight(flight, returnFlightList, index) {
    this._selectFLight(flight, returnFlightList, index);
    this.selectedFlight.rFprice = flight.selected ? flight.price : 0;
  }

  private _selectFLight(flight, flightList, index) {
    flight.selected = !flight.selected;
    for (var i = 0; i < flightList.length; i++) {
      if (i != index) flightList[i].selected = false;
    }
  }
}
